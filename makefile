SRC_FILES := $(wildcard *.c)
OBJ_DIR := ./target/object
OBJ_FILES := $(patsubst %.c,$(OBJ_DIR)/%.o,$(SRC_FILES))
LDFLAGS := -lm
CFLAGS := -ansi -pedantic -Wall -Wextra

mastermind: $(OBJ_FILES)
	gcc $(LDFLAGS) -g -o ./target/$@ $^

$(OBJ_DIR)/%.o: %.c
	gcc $(CFLAGS) -c -g -o $@ $<


CFLAGS = -pedantic -ansi -Wall -Wextra




