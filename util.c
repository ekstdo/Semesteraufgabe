#include "util.h"
#include <stdio.h>
#include <string.h>

/*
 * In dieser Datei sind alle IO-utility funktionen (IO = Input Output)
 *
 * z.B. flush oder safe_read
 *
 */

/* flush, um den Puffer bei getchar zu leeren */
void flush(){
  while (getchar() != '\n') { }
}

/* weil scanf("%s") nicht safe ist */
int safe_read(char *str, int limit){
  int c = getchar();
  int i = 0;
  while (c != '\n'){
    if (i > limit - 1){
      str[i] = '\0';
      flush();
      return TO_MANY_CHARS;
    }
    if (c == EOF)
      return EOF;

    str[i++] = c;
    c = getchar();
  }
  str[i] = '\0';

  return 0;
}

  /* print character arrays als Liste an Zahlen */
void print_charr(char* arr, int n){
  int i;
  if (n == 0) return;

  printf("[%i", arr[0]);
  for (i = 1; i < n; i ++) {
    printf(", %i", arr[i]);
  }

  printf("]");
}

  /* print int array */
void print_arr(int* arr, int n){
  int i;
  if (n == 0) return;

  printf("[%i", arr[0]);
  for (i = 1; i < n; i ++) {
    printf(", %i", arr[i]);
  }

  printf("]");
}

/* macht nen pop up overlay mit text und n zeilen drüber */
void popup(const char *text, int n){
  const char enter[2] = "\n";
  char copy_text[500] = "";
  char *line;
  strcpy(copy_text, text);
  line = strtok(copy_text, enter);

  printf("\033[%iA\033[12C\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D", n, "");
  while(line != NULL) {
    printf("\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D", line);
    line = strtok(NULL, enter);
  }
  printf("\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D\033[%iB", "", n);
}

char *string_repeat(char *str, int n) {
  int i;
  char *destination = calloc(n *strlen(str) + 1, sizeof(char));
  if (destination == NULL) {
    free(destination);
    return NULL;
  }
  for (i = 0; i < n; i++) {
    strcat(destination, str);
  }
  return destination;
}

#define LINE_SIZE 100

int parse_file(const char *filename, char **lines){
  FILE *file = fopen(filename, "r");
  char *line;
  int i = -1;
  char i_str[10] = "0:";

  if (!file){
    return 1;
  }

  line = malloc(LINE_SIZE * sizeof(char));
  while (fgets(line, LINE_SIZE, file) != NULL){
    if (prefix(line, i_str)){
      i ++;
      sprintf(i_str, "%i:", i + 1);
      continue;
    }
    strcat(lines[i], line);
  }

  fclose(file);
  if(line){
    free(line);
  }

  return 0;
}

int prefix(char *str, char *pre){
  return strncmp(str, pre, strlen(pre)) == 0;
}

/* init funktion für erstellen von ner Liste an Zeilen */
int init_lines(char ***lines, int end, int num){
  int i = 0;
  *lines = calloc(end, sizeof(char*));
  for (; i<end; i++){
    (*lines)[i] = calloc(num, sizeof(char));
    if ((*lines)[i] == NULL){
      return 1;
    }
  }
  return 0;
}

/* verwendet ANSI ESC codes, um zu bestimmen, wo man weiter printen soll */
void move_cursor(int x, int y){
  if (y < 0){
    printf("\033[%iA", -y);
  } else {
    printf("\033[%iB", y);
  }

  if (x < 0){
    printf("\033[%iD", -x);
  } else {
    printf("\033[%iC", x);
  }
}

int max(int x, int y){
	return x > y ? x : y;
}

#ifdef _WIN64
   /*define something for Windows (64-bit)*/
#elif _WIN32
   /*define something for Windows (32-bit)*/
#elif __APPLE__
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE && TARGET_OS_SIMULATOR
        /* define something for simulator
        // (although, checking for TARGET_OS_IPHONE should not be required).
    #elif TARGET_OS_IPHONE && TARGET_OS_MACCATALYST*/
        /* define something for Mac's Catalyst*/
    #elif TARGET_OS_IPHONE
        /* define something for iphone  */
    #else
        #define TARGET_OS_OSX 1
        /* define something for OSX*/
    #endif
#elif __linux
    /* linux*/
#elif __unix /* all unices not caught above*/
    /* Unix*/
#elif __posix
    /* POSIX */
#endif
