0: MENU_SCENE
Select "Start" and press enter to start the game
Select "Options" and press enter to select game options
Select "Exit" and press enter to exit the game
You can navigate with w/a/s/d on all platforms or with
1: GAME_SELECTION
Select "New game" to start a new game
or select a previously saved game
 
press d or - to delete the selected save state
2: PRE_GAME
Here you can change the settings of the game
you'll be playing

To change your name, first press enter before
typing

To add a color to the "Colors" option, just
press +r to add a random color or + followed by the ANSI
esc number. To delete colors press -
 
type "grey"/"gray" to make a grey colorscheme

3: GAME
If you don't know how this game works, go back and
switch to the tutorial mode by pressing enter 
multiple times at "Single player". (Until you reach
"Tutorial mode")

 Goal is to guess all the correct
colors and their correct positions.
>>>The time counts as soon as your turn starts<<<
