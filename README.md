# Semesteraufgabe

Die krasseste MasterMind implementation in ANSI C, die je existiert hat (yesyes, lol)

![screenshot](./screen.png)

<figure class="video_container">
  <iframe src="https://cdn.discordapp.com/attachments/453644038822952960/942523823532703814/MastermindX.mp4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

**!!! Nerd font wird empfohlen für beste experience** (ist aber nicht notwendig)

So wie: Windows terminal auf windows, kitty als Terminal auf Linux

## Compilen

Wenn ihr make habt (ist auf den meisten MacOs und Linux systemen schon drauf):

```
mkdir ./target/object
make
```

Wenn ihr kein make haben wollt:

```
gcc arr.c dir.c main.c util.c -Wall -Wextra -pedantic -ansi -o ./target/mastermind```
```

Die Binärdatei ist dann im `target` Ordner

also dann 

**Linus und MacOs**:

```
./target/mastermind
```

**Windows**: (je nachdem wie die Datei nun heißt)

```
.\target\mastermind.exe
```



## Für Mo:

um hiermit zu arbeiten, erstellt einen Ordner (z.B. Semesteraufgabe) und

```
cd Semesteraufgabe               # in den Ordner reingehen
git init                         # git initialisieren
git remote add origin https://gitlab.com/ekstdo/Semesteraufgabe.git
git branch -M main 
git pull origin main             # holt (pull) sich die Dateien vom Server
```

um dann Code wieder hochzuladen:

```
git add .
git commit -m "Woah, hab voll viel krasses geändert"
git push origin main
```



## Roadmap

siehe [Trello](https://trello.com/b/6hu7IJy0/semesteraufgabe)


