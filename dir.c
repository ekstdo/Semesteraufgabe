#include "dir.h"
#include "util.h"

#include <ctype.h>

char up = 'w';
char down = 's';
char left = 'a';
char right = 'd';

/* handeled input für Pfeiltaste oben/unten, bzw. w/s */
int up_down_input (char *input, int *value, int max) {
  Arrow arr = NOTHING;
  int skip;
  if (prefix(input, "\033[A")) {
    arr = UP;
    skip = 3;
  } else if (prefix(input, "\033[B")) {
    arr = DOWN;
    skip = 3;
  } else if (input[0] == up) {
    arr = UP;
    skip = 1;
  } else if (input[0] == down) {
    arr = DOWN;
    skip = 1;
  }
  if (*value > 0 && arr == UP) {
    (*value)--;
  } else if (*value < max -1 && arr == DOWN) {
    (*value)++;
  }
  if (arr) {
    up_down_input(input+skip, value, max);
    return 1;
  } else {
    return 0;
  }
}

/* handeled input für alle Richtungen */
int all_input (char *input, int *xvalue, int *yvalue, int xmax, int ymax) {
  Arrow arr = NOTHING;
  int skip;
  if (prefix(input, "\033[A")) {
    arr = UP;
    skip = 3;
  } else if (prefix(input, "\033[B")) {
    arr = DOWN;
    skip = 3;
  } else if (prefix(input, "\033[C")) {
    arr = RIGHT;
    skip = 3;
  } else if (prefix(input, "\033[D")) {
    arr = LEFT;
    skip = 3;
  } else if (input[0] == up) {
    arr = UP;
    skip = 1;
  } else if (input[0] == down) {
    arr = DOWN;
    skip = 1;
  } else if (input[0] == left){
    arr = LEFT;
    skip = 1;
  } else if (input[0] == right){
    arr = RIGHT;
    skip = 1;
  }

  if (*xvalue > 0 && arr == LEFT) {
    (*xvalue)--;
  } else if (*xvalue < xmax -1 && arr == RIGHT) {
    (*xvalue)++;
  } else if (*yvalue > 0 && arr == UP) {
    (*yvalue)--;
  } else if (*yvalue < ymax -1 && arr == DOWN) {
    (*yvalue)++;
  }

  if (arr) {
    all_input(input+skip, xvalue, yvalue, xmax, ymax);
    return 1;
  } else {
    return 0;
  }
}

/* handeled input und setzt auch Farben ein */
int all_color_input (char *input,
                     int *xvalue, int *yvalue,
                     int xmax, int ymax, int max_color_value,
                     int **board, int (*check)(int x, int y)) {
  Arrow arr = NOTHING;
  int skip;
  if (isdigit(*input)) {
    long number;
    number = strtol(input, &input, 10);
    if (check(*xvalue, *yvalue) && number < max_color_value) {
      board[*yvalue][*xvalue] = number;
    }
  }

  if(check(*xvalue, *yvalue)){
    while (*input == '+') {
      board[*yvalue][*xvalue] = (board[*yvalue][*xvalue] + 1) % max_color_value;
      input ++;
    }

    /* doppeltes Modulo, damit es auch negative Zahlen positiv macht */
    while (*input == '-') {
      board[*yvalue][*xvalue] = ((board[*yvalue][*xvalue] - 1) % max_color_value + max_color_value) % max_color_value;
      input ++;
    }
  }

  if (prefix(input, "\033[A")) {
    arr = UP;
    skip = 3;
  } else if (prefix(input, "\033[B")) {
    arr = DOWN;
    skip = 3;
  } else if (prefix(input, "\033[C")) {
    arr = RIGHT;
    skip = 3;
  } else if (prefix(input, "\033[D")) {
    arr = LEFT;
    skip = 3;
  } else if (input[0] == up) {
    arr = UP;
    skip = 1;
  } else if (input[0] == down) {
    arr = DOWN;
    skip = 1;
  } else if (input[0] == left){
    arr = LEFT;
    skip = 1;
  } else if (input[0] == right){
    arr = RIGHT;
    skip = 1;
  }

  if (*xvalue > 0 && arr == LEFT) {
    (*xvalue)--;
  } else if (*xvalue < xmax -1 && arr == RIGHT) {
    (*xvalue)++;
  } else if (*yvalue > 0 && arr == UP) {
    (*yvalue)--;
  } else if (*yvalue < ymax -1 && arr == DOWN) {
    (*yvalue)++;
  }

  if (arr) {
    all_color_input(input+skip, xvalue, yvalue, xmax, ymax, max_color_value, board, check);
    return 1;
  } else {
    return 0;
  }
}
