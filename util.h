#ifndef UTIL_LOADED
#define UTIL_LOADED

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TO_MANY_CHARS 2

void flush();
int safe_read(char *str, int limit);
void print_charr(char* arr, int n);
void popup(const char *text,int n);
int parse_file(const char *filename, char **lines);
int prefix(char *str, char *pre);
int init_lines(char ***lines, int end, int num);
void move_cursor(int x, int y);
int max(int x, int y);
char *string_repeat(char *str, int n);
#endif
