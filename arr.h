#ifndef ARR_LOADED
#define ARR_LOADED

#include <stdlib.h>
#include <string.h>

#define INIT_SIZE 20


typedef struct {
	int len;
	int elementSize;
	char* bytes;
	int cap;
} ArrayList;

#define init_list(type) (init_list_(sizeof(type)))
ArrayList init_list_(int elementSize);

#define push(list, el) (push_(&list, el))
void push_(ArrayList *list, void* el);

#define pop(list, type) (*(type*) pop_(&list))
void *pop_(ArrayList *list);

void destroy(ArrayList list);

void push_int(ArrayList *list, int el);
void append(ArrayList *list, void* el_list, int n);

int in(ArrayList *list, void* el);
#define get(list, i, type) (i < 0 ? (((type*)list.bytes)[list.len + i]) : (((type*)list.bytes)[i]))
int str_in(ArrayList *list, char* el);

#define set(list, i, val, type) {((type*)list.bytes)[i] = val;}
ArrayList clone(ArrayList *list);


int swap(ArrayList *list, int a, int b);
void sort_by(ArrayList *list, int (*cmp)(void* a, void *b));
int cmp_float(float *a, float *b);
int cmp_int(int *a, int *b);


#endif
