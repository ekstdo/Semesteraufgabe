#include <time.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "arr.h"
#include "util.h"
#include "dir.h"

#define DEBUG 0
#define DEBUG_GAME 0
#define NUMBER_TO_BIG 5
#define MENU_SELECTION_MAX 5
#define NUM_LOADABLE_STATES 9
#define MAX_NAME_LENGTH 20


#define err_(x) if(x != 0) return x;
#define report(x) if(x != 0) break;

typedef enum {
  WON = 1,
  LOST = 2,
  RUNNING = 0
} Outcome;

/* Zustand eines Spielbretts */
typedef struct {
  int **board; /* dynamisch allociert, weil die Brettgröße vom Spieler bestimmbar sein soll */
  int *red_pins;
  int *white_pins;
  int *solution;
  time_t timestamp;
  time_t timeacc;   /* rechnet die Zeit zusammen */
  Outcome outcome;
  int lines;
  int points; /* Punkte werden wie folgt ermittelt: Score = rounds - (round + timeacc / h) + red_pins + white_pins / 2 */
  int bot; /* integer, der verschiedene Zustände einnehmen kann */
} MasterMind;

typedef enum {
  MENU_SCENE = 0,
  GAME_SELECTION = 1,
  PRE_GAME = 2,
  GAME = 3,
  STATS = 4,
  FREE_DRAW = 5,
  OPTIONS = 6
} Scene;

typedef char Color[16];
typedef char Name[MAX_NAME_LENGTH];


/* Datenstruktur, in der alle Optionen stehen */
typedef struct {
  ArrayList player_names;
  int colors;
  int rounds; /* Anzahl der Runden */
  int goal; /* Anzahl der zu erratenen Steckköpfe */
  int num_players; /* Einzelspieler oder zu 2. */
  int selection;
  int duplicates; /* ob die Lösung doppelte Farben haben darf oder nicth */
  int counting_alg; /* verändert die Zählweise */
  ArrayList colormap;
} PreGameOptions;




/* Zustand, der geladen werden kann (also Bretter, Zeitstempel und Optionen) */
typedef struct {
  Name name;
  time_t timestamp;
} LoadableState;


typedef struct {
  Name name;
  int points;
  int max_points;
  int games_played;
  int games_won;
  int games_lost;
  int lines_submitted;
  int time_spent;
} Statistics;

typedef struct {
  Scene scene;
  ArrayList state_history;
  struct GameData {
    struct MenuData { int selection; } menu;
    struct SelectionState { LoadableState loadable_states[NUM_LOADABLE_STATES]; int selected; int len; } selection;
    PreGameOptions pregame;
    struct Game {
      MasterMind *game;
      int round;
      PreGameOptions options;
      int pause;
      int pause_selection;
      int select_color; /* -1, wenn keine Farbe ausgewählt ist */
      int selectedX;
      int selectedY;
      int preselect;
      int disclose;
      int current_playing;
      int ended;
      int loaded; /* welche Speicherdatei geladen wurde */
      int tutorial;
      char* msg;
    } game;
    struct FreeDrawing {
      MasterMind board;
      int select_color; /* -1, wenn keine Farbe ausgewählt ist */
      int selectedX;
      int selectedY;
    } free_drawing;
    struct StatsState {
      int selection;
      int neg;
    } stats;
    struct OptionState {
      int selection;
    } options;
  } data;
  ArrayList stats;
} GameState;








/* outcome, help und pause sollten als overlay dargestellt werden und sind daher keine Szenen*/

GameState state;

int read_stats_file();

void init_state(){
  struct SelectionState selection;
  struct GameData data;
  struct MenuData menu = { 0 };   /* festlegen der Daten des Hauptmenüs */

  state.scene = MENU_SCENE;

  selection.selected = 0;
  selection.len = 0;
  data.selection = selection;
  data.menu = menu;

  state.data = data;
  state.state_history = init_list(int);
}

void free_board(MasterMind game) {
  int i;
  for (i = 0; i < state.data.game.options.rounds; i++) {
    free(game.board[i]);
  }
  free(game.board);

  free(game.red_pins);
  free(game.white_pins);

  free(game.solution);
}

int text_input = 0;
char prompt[50] = "";

int (*init_scene[7])();
int (*destroy_scene[7])();

void cleanup(){
  destroy(state.state_history);
  (destroy_scene[state.scene])();
}

int switch_scene(Scene scene){
  int earlier_state = state.scene;
  int res;
  push(state.state_history, &state.scene);
  state.scene = scene;
  res = (init_scene[state.scene])();
  (destroy_scene[earlier_state])();
  return res;
}

int pop_scene(){
  int scene = pop(state.state_history, int);
  (destroy_scene[state.scene])();
  state.scene = scene;
  return (init_scene[state.scene])();
}

int init_menu(){
  state.data.menu.selection = 0;
  return 0;
}

int destroy_menu(){
  return 0;
}

int print_menu(){
  /*                        ┌─┐
   * ┌──┮──━┓             ┌─┘  ─┐                    ┌─┐           ┌─┐
   * │  │   │             └─┐ ┌─┘                    └─┘           │ │
   * │      │ ┌──┬─┐ ┌────┐ │ │ ┌──── `┌┬──┐ ┌──┬──┐ ┌─┐ ┌──┬─┐ ┌──┤ │
   * │ │  │ │ │  │ │ │ ───┤ │ │ │ ── │ │ ┌─┘ │  │  │ │ │ │  │ │ │  │ │
   * │ │  │ │ │ │  │ ├──  │ │ │ │  ──┤ │ │   │ │ │ │ │ │ │    │ │ │  │
   *─┴─┴──┴─┴─┴──┴─┴─┴────┴─┴─┴─┴────┴─┴─┴───┴─┴─┴─┴─┴─┴─┴─┴──┴─┴──┴─┴─
   */

  const char *menu_selection_text[] = {"Start ", "Options ", "Free drawing board ", "Stats ", "Exit "};
  int i;

  printf("     \033[38;5;33m        \033[0m\033[38;5;75m               ┌─┐\033[0m\n"
         "     \033[38;5;33m┌──┮───┐\033[0m\033[38;5;75m             ┌─┘  ─┐           \033[0m\033[38;5;575m         \033[38;5;155m┌─┐\033[38;5;575m           ┌─┐\033[0m\n"
         "     \033[38;5;33m│  │   │\033[0m\033[38;5;75m             └─┐ ┌─┘           \033[0m\033[38;5;575m         \033[38;5;155m└─┘\033[38;5;575m           │ │\033[0m\n");
  printf("     \033[38;5;33m│      │\033[0m\033[38;5;75m ┌──┬─┐ ┌────┐ │ │ ┌──── `┌┬──┐\033[0m\033[38;5;575m ┌──┬──┐ ┌─┐ ┌──┬─┐ ┌──┤ │\033[0m\n"
         "     \033[38;5;33m│ │  │ │\033[0m\033[38;5;75m │  │ │ │ ───┤ │ │ │ ── │ │ ┌─┘\033[0m\033[38;5;575m │  │  │ │ │ │  │ │ │  │ │\033[0m\n");
  printf("     \033[38;5;33m│ │  │ │\033[0m\033[38;5;75m │ │  │ ├──  │ │ │ │  ──┤ │ │  \033[0m\033[38;5;575m │ │ │ │ │ │ │    │ │ │  │\033[0m\n"
         "    ─\033[38;5;33m┴─┴──┴─┴\033[0m\033[38;5;75m─┴──┴─┴─┴────┴─┴─┴─┴────┴─┴─┴──\033[0m\033[38;5;575m─┴─┴─┴─┴─┴─┴─┴─┴──┴─┴──┴─┴─\033[0m\n\n\n");

  printf("\n    \033[1mIf not already enabled, please view in full screen mode!!!\033[0m\n\n\n");
  for (i = 0; i < 5; i ++){
    printf("  [%c] %s\n\n", i == state.data.menu.selection ? 'X' : ' ', menu_selection_text[i]);
  }

  printf("\n\n\n\n\n");
  return 0;
}

int input_menu(char* input){
  if (up_down_input(input, &state.data.menu.selection, MENU_SELECTION_MAX)) {
    return 0;
  }
  if (input[0] != 0) {
    return 0;
  }


  switch (state.data.menu.selection){
    case 0:
      switch_scene(GAME_SELECTION);
      break;
    case 1:
      switch_scene(OPTIONS);
      break;
    case 2:
      switch_scene(FREE_DRAW);
      break;
    case 3:
      switch_scene(STATS);
      break;
    case 4:
      return 1;
  }
  return 0;
}


int check_loadable_games(){
  int i;
  for (i = 0; i < NUM_LOADABLE_STATES; i++){
    FILE *game_file;
    char filename[20];
    sprintf(filename, "gamestate-%i.mm", i); /* snprintf darf nicht verwendet werden, weil ANSI */

    if ((game_file = fopen(filename, "rb"))){
      int cols, lines;
      fread(&lines, sizeof(int), 1, game_file);
      fread(&cols , sizeof(int), 1, game_file);
      fseek(game_file, sizeof(int) * 6, SEEK_SET);
      fread(state.data.selection.loadable_states[i].name, sizeof(Name), 1, game_file);
      fseek(game_file,
          sizeof(int) * 6 + sizeof(Name) +
          sizeof(int) * cols * lines +
          sizeof (int) * cols +
          2 * sizeof(int) * lines + sizeof(int) +
          sizeof(int), SEEK_SET);
      fread(&state.data.selection.loadable_states[i].timestamp, sizeof(time_t), 1, game_file);
      fclose(game_file);
    } else {
      break;
    }
  }

  state.data.selection.len = i;
  return 0;
}

int init_selection(){
  state.data.menu.selection = 0;
  check_loadable_games();
  return 0;
}

int destroy_selection(){
  return 0;
}

int print_selection(){
  int i;
  char timestamp[26];
  for (i = 0; i < NUM_LOADABLE_STATES; i ++){
    if (state.data.selection.len == i){
      printf("%s    ┌───────────────────────────────────────────────────────────────┐\n"
             "    │ NEW GAME                                                      │\n"
             "    └───────────────────────────────────────────────────────────────┘\033[0m\n",  i == state.data.selection.selected ? "" : "\033[90m");
      break;
    }

    strftime(timestamp, 25, "%Y-%m-%d %H:%M:%S", localtime(&state.data.selection.loadable_states[i].timestamp));
    printf("    %s┌───────────────────────────────────────────────────────────────┐\n"
           "    │ Game %i, Player: %-20s      %s │\n"
           "    └───────────────────────────────────────────────────────────────┘\n\033[0m",
          i == state.data.selection.selected ? "" : "\033[90m",
          i,
          state.data.selection.loadable_states[i].name,
          timestamp);
  }
  return 0;
}

int init_board(MasterMind *game, int index);

int load_game(int index){
  FILE *file;
  char filename[20];
  int i, j;
  int timesize;
  state.data.game.tutorial = 0;
  sprintf(filename, "gamestate-%i.mm", index); /* snprintf darf nicht verwendet werden, weil ANSI  */
  file = fopen(filename, "rb");
  fread(&state.data.game.options.rounds, sizeof(int), 1, file);
  fread(&state.data.game.round, sizeof(int), 1, file);
  fread(&state.data.game.options.goal  , sizeof(int), 1, file);
  fread(&state.data.game.options.colors, sizeof(int), 1, file);
  fread(&state.data.game.options.num_players, sizeof(int), 1, file);
  fread(&state.data.game.current_playing, sizeof(int), 1, file);

  state.data.game.options.player_names = init_list(Name);
  state.data.game.game = malloc(state.data.game.options.num_players * sizeof(MasterMind));
  for (i = 0; i < state.data.game.options.num_players; i ++){
    Name player_name = "";
    fread(player_name, sizeof(Name), 1, file);
    push(state.data.game.options.player_names, &player_name);

    init_board(&state.data.game.game[i], i);

    for (j = 0; j < state.data.game.options.rounds; j ++){
      fread(state.data.game.game[i].board[j], sizeof(int), state.data.game.options.goal, file);
    }

    fread(state.data.game.game[i].solution,   sizeof(int), state.data.game.options.goal  , file);
    fread(state.data.game.game[i].red_pins,   sizeof(int), state.data.game.options.rounds, file);
    fread(state.data.game.game[i].white_pins, sizeof(int), state.data.game.options.rounds, file);
    fread(&state.data.game.game[i].outcome, sizeof(int), 1, file);
    fread(&timesize, sizeof(int), 1, file);
    fread(&state.data.game.game[i].timestamp, sizeof(int), timesize, file);
    fread(&state.data.game.game[i].bot, sizeof(int), 1, file);
  }

  state.data.game.options.colormap = init_list(Color);

  for (i = 0; i < state.data.game.options.colors; i++){
    Color a = "";
    fread(a, sizeof(Color), 1, file);
    push(state.data.game.options.colormap, &a);
  }

  fread(&state.data.game.ended, sizeof(int), 1, file);
  fread(&state.data.game.options.counting_alg, sizeof(int), 1, file);

  fclose(file);
  state.data.game.loaded = index;
  switch_scene(GAME);
  return 0;
}



int input_selection(char* input){
  if (up_down_input(input, &state.data.selection.selected, state.data.selection.len + 1)) {
    return 0;
  }
  if (input[0] == 'd' || input[0] == '-'){
    char filename[30];
    char filename2[30];
    int count;
    sprintf(filename, "gamestate-%i.mm", state.data.selection.selected);
    remove(filename);
    for (count = state.data.selection.selected + 1; count < state.data.selection.len; count++){
      sprintf(filename2, "gamestate-%i.mm", count);
      rename(filename2, filename);
      strcpy(filename, filename2);
    }
    check_loadable_games();
  }
  if (input[0] == 0 && state.data.selection.selected == state.data.selection.len)
    switch_scene(PRE_GAME);
  else if (input[0] == 0)
    err_(load_game(state.data.selection.selected));

  return 0;
}

#define DEFAULT_AMOUNT_COLORS 6
#define DEFAULT_AMOUNT_ROUNDS 12
#define DEFAULT_AMOUNT_GOAL 4
#define NUM_OPTIONS 9
#define DEFAULT_AMOUNT_PLAYERS 1


Color default_colors[50] = { "\033[48;5;70m", "\033[48;5;27m", "\033[48;5;57m", "\033[48;5;174m", "\033[48;5;87m", "\033[48;5;172m", "\033[48;5;36m", "\033[48;5;124m", "\033[48;5;54m", "\033[48;5;227m"};
Color grey_scale[50] = { "\033[48;5;255m", "\033[48;5;251m", "\033[48;5;247m", "\033[48;5;243m", "\033[48;5;239m", "\033[48;5;235m", "\033[48;5;232m"};



int init_pregame(){
  Name zero_init = "";
  int i;
  state.data.pregame.selection = 0;
  state.data.pregame.rounds = DEFAULT_AMOUNT_ROUNDS;
  state.data.pregame.colors = DEFAULT_AMOUNT_COLORS;
  state.data.pregame.goal = DEFAULT_AMOUNT_GOAL;
  state.data.pregame.num_players = DEFAULT_AMOUNT_PLAYERS;
  state.data.pregame.duplicates = 1;
  state.data.pregame.player_names = init_list(Name);
  state.data.pregame.colormap = init_list(Color);
  state.data.pregame.counting_alg = 0;
  for (i = 0; i < 20; i++){
    push(state.data.pregame.player_names, &zero_init);
  }

  append(&state.data.pregame.colormap, default_colors, 10);
  return 0;
}

/* free the pregame */
int destroy_pregame(){
  destroy(state.data.pregame.player_names);
  destroy(state.data.pregame.colormap);
  return 0;
}

#define select(i) (state.data.pregame.selection == (i) ? "\033[30;47m": "")
#define player0 (get(state.data.pregame.player_names, 0, Name))
#define player(n) (get(state.data.pregame.player_names, n, Name))
#define press_enter_text(i) (strlen(player(i)) == 0 ? "\033[38;5;243mPress enter to insert a name:\033[0m" : player(i))

char count_alg_names[50][50] = { "standard", "by input", "positional" };

int print_pregame(){
  char *a;
  int i;

  printf("    %sName: [%s%s]\033[0m\n\n", select(0), press_enter_text(0), select(0));
  printf("    %sNumber of rounds: %i\033[0m\n\n", select(1), state.data.pregame.rounds);
  printf("    %sNumber of different colors: %i\033[0m\n\n",select(2), state.data.pregame.colors);
  printf("    %sNumber of slots to guess: %i\033[0m\n\n", select(3), state.data.pregame.goal);

  printf("    %sDuplicate colors: %s\033[0m\n\n", select(4), state.data.pregame.duplicates ? "enabled" : "disabled");

  printf("    %sColors:  ", select(5));
  for (i = 0; i < state.data.pregame.colormap.len; i ++){
    printf("%s   ", get(state.data.pregame.colormap, i, Color));
  }
  printf("%s \033[0m\n\n", select(5));

  printf("    %sSuper Mastermind%c\033[0m\n\n", select(6), state.data.pregame.colors == 8 && state.data.pregame.goal == 5 && state.data.pregame.rounds == 12? '!' : '?');
  
  printf("    %sCounting algorithm: %s\033[0m\n\n", select(7), count_alg_names[state.data.pregame.counting_alg]);

  printf("    %sNumber of players: %s\033[0m\n\n", select(8), state.data.pregame.num_players == 0? "Tutorial mode" : state.data.pregame.num_players == 1? "Singleplayer (against CPU)" : "Multiplayer");


  for (i = 1; i < max(state.data.pregame.num_players, 1); i++){
    printf("    %sName Player %i: [%s]\033[0m\n\n", select(8 + i), i + 1, press_enter_text(i));
  }

  a = select(8 + max(state.data.pregame.num_players, 1));
  printf("                %s┌───────────────────┐\033[0m\n"
         "                %s│       PLAY       │\033[0m\n"
         "                %s└───────────────────┘\033[0m\n\n", a, a, a);
  return 0;
}

int input_pregame(char* input){
  if (text_input && state.data.pregame.selection == 0){
    strncpy(player0, input, MAX_NAME_LENGTH - 1);
    prompt[0] = '\0';
    text_input = 0;
    return 0;
  }

  if (text_input && state.data.pregame.num_players > 1){
    strncpy(player(state.data.pregame.selection - 8), input, MAX_NAME_LENGTH - 1);
    prompt[0] = '\0';
    text_input = 0;
    return 0;
  }

  if (up_down_input(input, &state.data.pregame.selection, NUM_OPTIONS + max(state.data.pregame.num_players, 1))) {
    return 0;
  }

  if (state.data.pregame.selection == 0) {
    /* name */
    text_input = 1;
    strcpy(prompt, "text > ");
  } else if (state.data.pregame.selection == 1) {
    int result = strtol(input, NULL, 10);
    if (result == -1)
      return NUMBER_TO_BIG;
    state.data.pregame.rounds = result == 0? DEFAULT_AMOUNT_ROUNDS : result;
  } else if (state.data.pregame.selection == 2) {
    int result = strtol(input, NULL, 10);
    if (result == -1)
      return NUMBER_TO_BIG;
    state.data.pregame.colors = result == 0? DEFAULT_AMOUNT_COLORS : result;
  } else if (state.data.pregame.selection == 3) {
    int result = strtol(input, NULL, 10);
    if (result == -1)
      return NUMBER_TO_BIG;
    state.data.pregame.goal = result == 0? DEFAULT_AMOUNT_GOAL : result;
  } else if (state.data.pregame.selection == 4){
    state.data.pregame.duplicates = !state.data.pregame.duplicates;
  } else if (state.data.pregame.selection == 5) {
    if (input[0] == '-'){
      int count = 1;
      if (isdigit(input[1])){
        count = strtol(input + 1, NULL, 10);
      }

      state.data.pregame.colormap.len -= count;
    }

    if (input[0] == '+'){
      int c;
      Color col = "";

      if (input[1] == 'r')
        c = rand() % 256;

      if (isdigit(input[1]))
        c = strtol(input + 1, NULL, 10);
      sprintf(col, "\033[48;5;%im", c);
      push(state.data.pregame.colormap, &col);
    }
    if (prefix(input, "grey") || prefix(input, "gray")){
      state.data.pregame.colormap.len = 0;
      append(&state.data.pregame.colormap, grey_scale, 7);
    }
  } else if (state.data.pregame.selection == 6) {
    state.data.pregame.goal = 5;
    state.data.pregame.colors = 8;
    state.data.pregame.rounds = 12;
  } else if (state.data.pregame.selection == 7) {
    state.data.pregame.counting_alg = (state.data.pregame.counting_alg + 1) % 3;
  } else if (state.data.pregame.selection == 8) {
    if (input[0] == 0){
      state.data.pregame.num_players = (state.data.pregame.num_players + 1) % 5;
    } else {
      int result = strtol(input, NULL, 10);
      if (result != -1)
        state.data.pregame.num_players = result;
    }
  } else if (state.data.pregame.selection >= 9 && state.data.pregame.selection < 8 + state.data.pregame.num_players) {
    text_input = 1;
    strcpy(prompt, "text > ");
  } else if (state.data.pregame.selection == (8 + max(state.data.pregame.num_players, 1)) && input[0] == 0){
    switch_scene(GAME);
  }
  return 0;
}

#undef select

#define quick_ret(X) { if (X == NULL) { free(X); return 1; } }
#define err_handle(X) {if (X != 0) { return X; }}


/* generiert eine random Lösung */
void random_solution(int *solution){
  int i;
  for (i = 0; i < state.data.game.options.goal; i++) {
    solution[i] = rand() % state.data.game.options.colors;
  }
}

/* generiert eine Lösung ohne doppelten Farben */
void random_solution_no_d(int *solution){
  int i;
  ArrayList a = init_list(int);
  for(i = 0; i < state.data.game.options.colors; i ++){
    push_int(&a, i);
  }

  for (i = 0; i < state.data.game.options.goal; i++) {
    int ind = rand() % a.len;
    if (a.len - 1 != ind)
      swap(&a, a.len - 1, ind);

    solution[i] = pop(a, int);
  }
}

const char bot_names[50][50] = { "Rando Bot", "Ultra Bot" };

int init_board(MasterMind *game, int index){
  int i, j;

  game->board = malloc(state.data.game.options.rounds * sizeof(int*));
  game->red_pins = calloc(state.data.game.options.rounds, sizeof(int));
  quick_ret(game->red_pins);
  game->white_pins = calloc(state.data.game.options.rounds, sizeof(int));
  quick_ret(game->white_pins);
  game->outcome = RUNNING;

  for (i = 0; i < 2; i++){
    if (strcmp(get(state.data.game.options.player_names, index, Name), bot_names[i]) == 0){
      game->bot = i + 1;
    }
  }


  for (i = 0; i < state.data.game.options.rounds; i++) {
    game->board[i] = malloc(state.data.game.options.goal * sizeof(int));
    for (j = 0; j < state.data.game.options.goal; j++) {
      game->board[i][j] = -1;
    }
  }

  game->timeacc = 0;

  game->solution = malloc(state.data.game.options.goal * sizeof(int));
  quick_ret(game->solution);

  if (state.data.game.options.duplicates)
    random_solution(game->solution);
  else
    random_solution_no_d(game->solution);
  return 0;
}

int init_new_game(){
  int j;

  state.data.game.loaded = -1;
  state.data.game.options = state.data.pregame;
  if (state.data.game.tutorial)
    state.data.game.options.num_players = 1;
  state.data.game.options.player_names = clone(&state.data.pregame.player_names);
  state.data.game.options.colormap = clone(&state.data.pregame.colormap);
  state.data.game.current_playing = 0;

  if(state.data.game.options.num_players > 1){
    state.data.game.preselect = 1;
    state.data.game.disclose = 1;
  } else {
    state.data.game.preselect = -1;
  }

  state.data.game.round = 0;

  state.data.game.game = malloc(state.data.game.options.num_players * sizeof(MasterMind));
  quick_ret(state.data.game.game);
  for (j = 0; j < state.data.game.options.num_players; j++){
    state.data.game.game[j].timestamp = time(NULL);
    init_board(&state.data.game.game[j], j);
  }
 
  state.data.game.ended = 0;
  return 0;
}

int init_game(){
  state.data.game.select_color = -1;
  state.data.game.selectedX = 0;
  state.data.game.selectedY = 0;
  state.data.game.pause = 0;
  state.data.game.pause_selection = 0;
  state.data.game.disclose = 0;
  state.data.game.tutorial = 0;


  state.data.game.msg = calloc(100,sizeof(char));

  state.stats = init_list(Statistics);
  err_handle(read_stats_file());


  if (get(state.state_history, state.state_history.len - 1, int) == PRE_GAME) {
    state.data.game.tutorial = state.data.pregame.num_players == 0;
    if (state.data.game.tutorial)
      state.data.game.options.num_players = 1;

    err_handle(init_new_game());
    state.data.game.game[0].timestamp = time(NULL);
  } else {
    state.data.game.preselect = -1;
  }

  return 0;
}

int destroy_game(){
  int j;
  free(state.data.game.msg);
  destroy(state.data.game.options.player_names);
  for (j = 0; j < state.data.game.options.num_players; j++)
    free_board(state.data.game.game[j]);

  free(state.data.game.game);
  destroy(state.stats);
  state.data.game.loaded = -1;
  return 0;
}


int draw_board(MasterMind game){
  int i, j; /* rows and columns */
  printf("    ");
  for (i = 0; i < state.data.game.options.rounds; i ++){
    char *white_pins;
    char *red_pins;
    char *pins;

    if (state.data.game.options.counting_alg != 2){
      white_pins = string_repeat("●", game.white_pins[i]);
      red_pins = string_repeat("●", game.red_pins[i]);
      pins = malloc((strlen(white_pins) + strlen(red_pins) + 10) * sizeof(char));
      sprintf(pins, "%s\033[91m%s", white_pins, red_pins);
      free(red_pins);
      free(white_pins);
    } else {
      pins = calloc(state.data.game.options.goal * 10, sizeof(char));
      for (j = 0; j < state.data.game.options.goal; j ++){
        if ((1 << j) & game.red_pins[i])
          strcat(pins, "\033[91m●");
        else if ((1 << j) & game.white_pins[i])
          strcat(pins, "\033[0m●");
        else
          strcat(pins, " ");
      }
    }

    if (i == 0){
      printf("┌");
      for (j = 0; j < state.data.game.options.goal - 1; j ++){
        printf("───┬");
      }
      printf("───┐");
      move_cursor(-(4 * state.data.game.options.goal + 1), 1);
    }


    for (j = 0; j < state.data.game.options.goal; j ++){
      int cell_state = game.board[i][j];
      printf("│%s   \033[0m", cell_state == -1?"": get(state.data.game.options.colormap, cell_state, Color));
    }
    printf("│%s\033[0m", pins);
    free(pins);
    if (state.data.game.options.counting_alg != 2)
      move_cursor(-(4 * state.data.game.options.goal + 1 + game.white_pins[i]+game.red_pins[i]), 1);
    else
      move_cursor(-(5 * state.data.game.options.goal + 1),1);
    printf("├");

    for (j = 0; j < state.data.game.options.goal - 1; j ++){
      printf("───┼");
    }
    printf("───┤");
    move_cursor(-(4 * state.data.game.options.goal + 1), 1);
  }

  if (game.outcome) {
    for (j = 0; j < state.data.game.options.goal; j ++){
      int cell_state = game.solution[j];
      printf("│%s   \033[0m", cell_state == -1?"": get(state.data.game.options.colormap, cell_state, Color));
    }
  } else {
    for (j = 0; j < state.data.game.options.goal; j ++){
      printf("│ ? ");
    }
  }
  printf("│");
  move_cursor(-(4 * state.data.game.options.goal + 1), 1);
  printf("└");
  for(j = 0; j < state.data.game.options.goal - 1; j ++){
    printf("───┴");
  }
  printf("───┘");
  move_cursor(-(4 * state.data.game.options.goal + 1), 1);
  return 0;
}

int mark_selected(MasterMind game, int x, int y){
  int nx = state.data.game.options.goal;
  int cell_state = game.board[y][x];
  move_cursor( state.data.game.current_playing * (5 * state.data.game.options.goal + 2 + 5) + 5 + x * 4 , y* 2 -1 );
  printf("\033[30;47m%s───%s\033[0m",
      y == 0 ? (x == 0? "┌" : "┬") : (x == 0? "├" : "┼"),
      y == 0? (x == nx  - 1? "┐" : "┬"): (x == nx - 1 ? "┤" :"┼"));
  move_cursor(-5, 1);
  printf("\033[30;47m│%s   \033[30;47m│\033[0m", cell_state == -1?"": get(state.data.game.options.colormap, cell_state, Color));
  move_cursor(-5, 1);
  printf("\033[30;47m%s───%s\033[0m", x == 0? "├" : "┼", x == nx - 1 ? "┤" :"┼");
  move_cursor(0, -(y -state.data.game.options.rounds)* 2 + 5 );

#if DEBUG == 1
  int ny = state.data.game.options.rounds;
  printf("x: %i, y: %i, xn: %i, yn: %i", x, y, nx, ny);
#endif

  printf("\n\n\n\n");
  return 0;
}



#define select(x) (state.data.game.select_color == (x) ? "\033[30;47m": "")
#define select_pause(x) (state.data.game.pause_selection == (x) ? "\033[0m": "\033[30;47m")
#define select_pausea(x) (state.data.game.pause_selection == (x) ? "\033[0m": "")



void draw_color_menu(){
  int i;
  int offset = 0;
  int n = state.data.game.options.num_players;

  move_cursor(n * (5 + 6 * state.data.game.options.goal), -25);
  printf("┌─%sColors: \033[0m ───────────────┐", state.data.game.select_color != -1 ? "\033[30;47m": "");
  move_cursor(-27, 1);
  printf("│ %s┌───┐\033[0m                   │", select(0));
  move_cursor(-27, 1);

  for (i = 0; i < state.data.game.options.colors; i ++){
    char spaces[21] = "                     ";
    printf("│ %s│%s   \033[0m%s│\033[0m", select(i), get(state.data.game.options.colormap, i, Color), select(i));

    offset = printf(" (%i)", i);
    spaces[20 - offset - 1] = '\0';
    printf("%s│", spaces);
    move_cursor(-27, 1);
    if (i == state.data.game.options.colors - 1){
      printf("│ %s└───┘\033[0m                   │", select(i));
    } else {
      printf("│ %s%s├───┤\033[0m                   │", select(i), select(i + 1));
    }
    move_cursor(-27, 1);
  }
  printf("└─────────────────────────┘");
  move_cursor(- 25 - 7 * state.data.game.options.num_players * state.data.game.options.goal, 20);
}

int print_pause(){
  int n = 25;
  printf("\033[%iA\033[12C\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D", n, "");
  printf("\033[0;30m\033[47m    \033[1mPaused%-54s\033[0m\033[0m\033[1B\033[64D", "");
  printf("\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D", "");
  printf("\033[0;30m\033[47m             %s┌───────────────────┐\033[30;47m%-30s\033[0;0m\033[0m\033[1B\033[64D", select_pause(0), "");
  printf("\033[0;30m\033[47m             %s│       SAVE       │\033[30;47m%-30s\033[0;0m\033[0m\033[1B\033[64D", select_pause(0), "");
  printf("\033[0;30m\033[47m             %s└───────────────────┘\033[30;47m%-30s\033[0;0m\033[0m\033[1B\033[64D", select_pause(0), "");

  printf("\033[0;30m\033[47m             %s┌───────────────────┐\033[30;47m%-30s\033[0;0m\033[0m\033[1B\033[64D", select_pause(1), "");
  printf("\033[0;30m\033[47m             %s│      RESUME      │\033[30;47m%-30s\033[0;0m\033[0m\033[1B\033[64D", select_pause(1), "");
  printf("\033[0;30m\033[47m             %s└───────────────────┘\033[30;47m%-30s\033[0;0m\033[0m\033[1B\033[64D", select_pause(1), "");

  printf("\033[0;30m\033[47m             %s┌───────────────────┐\033[30;47m%-30s\033[0;0m\033[0m\033[1B\033[64D", select_pause(2), "");
  printf("\033[0;30m\033[47m             %s│       QUIT       │\033[30;47m%-30s\033[0;0m\033[0m\033[1B\033[64D", select_pause(2), "");
  printf("\033[0;30m\033[47m             %s└───────────────────┘\033[30;47m%-30s\033[0;0m\033[0m\033[1B\033[64D", select_pause(2), "");


  printf("\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D\033[%iB", "", n);
  return 0;
}

int multiplayer_color_selection(int player) {
  int n = 25;
  int i;
  int offset = 0, offset2 = 0, offset3 = 0;
  char *string1, *string2, *string3, *whitespace;
  int *arr = state.data.game.game[player].solution;
  whitespace = string_repeat(" ", 64 - (state.data.game.options.goal * 4 + 6));
  string1 = malloc(10 * (4 * state.data.game.options.goal + 2) * sizeof(char));
  quick_ret(string1);
  string2 = malloc(10 * (4 * state.data.game.options.goal + 2) * sizeof(char));
  quick_ret(string2);
  string3 = malloc(10 * (4 * state.data.game.options.goal + 2) * sizeof(char));
  quick_ret(string3);
  printf("\033[%iA\033[12C\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D", n, "");
  printf("\033[0;30m\033[47m    \033[1mChose color for player %-20s%-17s\033[0m\033[0m\033[1B\033[64D", get(state.data.game.options.player_names, player, Name), "");
  printf("\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D", "");

  for (i = 0; i < state.data.game.options.goal; i++) {
    if (i == 0) {
      offset  = sprintf(string1,"%s┌───", select_pause(0));
      offset2 = sprintf(string2,"%s│%s   %s", select_pause(0), get(state.data.game.options.colormap,arr[i], Color), select_pause(0));
      offset3 = sprintf(string3,"%s└───", select_pause(0));
    } else if (i == state.data.game.options.goal - 1) {
      offset  += sprintf(string1 + offset, "%s┬%s───┐%s", select_pausea(i), select_pause(i),"\033[30;47m");
      offset2 += sprintf(string2 + offset2,"%s│%s   %s│%s", select_pausea(i), get(state.data.game.options.colormap,arr[i], Color), select_pause(i),"\033[30;47m");
      offset3 += sprintf(string3 + offset3,"%s┴%s───┘%s", select_pausea(i), select_pause(i),"\033[30;47m");
    } else {
      offset  += sprintf(string1 + offset ,"%s┬%s───", select_pausea(i), select_pause(i));
      offset2 += sprintf(string2 + offset2,"%s│%s   %s", select_pausea(i), get(state.data.game.options.colormap,arr[i], Color), select_pause(i));
      offset3 += sprintf(string3 + offset3,"%s┴%s───", select_pausea(i), select_pause(i));
    }
  }
  printf("\033[0;30m\033[47m     %s%s", string1, whitespace);
  move_cursor(-64, 1);
  printf("\033[0;30m\033[47m     %s%s", string2, whitespace);
  move_cursor(-64, 1);
  printf("\033[0;30m\033[47m     %s%s", string3, whitespace);
  move_cursor(-64, 1);
  printf("\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D\033[%iB", "", n);

  free(string1);
  free(string2);
  free(string3);
  free(whitespace);
  return 0;
}

void disclosing(){
  int n = 25;
  printf("\033[%iA\033[12C\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D", n, "");
  printf("\033[0;30m\033[47m    \033[1mHidden! Waiting for player %i %-25s%-6s\033[0m\033[0m\033[1B\033[64D",
      state.data.game.preselect == 0 ? state.data.game.options.num_players : state.data.game.preselect,
      get(state.data.game.options.player_names, (state.data.game.preselect - 1 + state.data.game.options.num_players) % state.data.game.options.num_players, Name),
      "");
  printf("\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D", "");
  printf("\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D\033[%iB", "", n);
}

void print_end(){
  int i;
  int n = 25;
  printf("\033[%iA\033[12C\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D", n, "");
  printf("\033[0;30m\033[47m    \033[1mThe END! %-51s\033[0m\033[0m\033[1B\033[64D", "");

  for (i = 0; i < state.data.game.options.num_players; i++){
    printf("\033[0;30m\033[47m    Player %i: %-20s     | %5i%18s\033[0;0m\033[0m\033[1B\033[64D", i + 1, get(state.data.game.options.player_names, i, Name), state.data.game.game[i].points, "");
  }
  printf("\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D", "");
  printf("\033[0;30m\033[47m    %-60s\033[0;0m\033[0m\033[1B\033[64D\033[%iB", "", n);
}

void print_tutorial(){
  switch (state.data.game.tutorial){
    case 1:
      popup("Hi and welcome to the tutorial of Master Mind", 15);
      break;
    case 2:
      popup("This is a logic based board game\n \nyou can press t to revert back\nin the tutorial", 15);
      break;
    case 3:
      popup("You can press enter on a cell to choose the color you want\nor you can type the number next to the\ncolor and press enter", 15);
      break;
    case 5:
      popup("Great! You can also combine numbers with movement\nby typing \"d2d3\" e.g.", 15);
      break;
    case 8:
      popup("The goal of this game is to guess the 4\ngenerated colors at the bottom correctly", 15);
      break;
    case 9:
      popup("Now fill in the rest of the row\nand submit by pressing '.' <enter>", 15);
      break;
    case 11:
      popup("Now you can see white and red pins on the side\n \n"
            "White pin: One of your selected colors is correct.\n"
            "Red pin: One of your selected color is correct AND the\nposition is correct.\n \n"
            "\nIf the solution was Green Blue Violet Orange and you Type\nGreen Green Cyan Cyan\n"
            "There will be one red pin because there can't be more\npins of a color than the color in the solution.\n"
            "In case there is no pin: None of your selected colors is\ncorrect.", 25);
      break;
    case 12:
      popup("Good luck!\n \nHave fun playing! \\^-^/", 15);
      break;
    default:
      break;
  }
}

int print_game(){
  int i;
  int amount_of_new_lines = 3 + state.data.game.options.rounds * 2;
  char *new_lines = malloc((amount_of_new_lines + 1) * sizeof(char));
  int current_playing = state.data.game.current_playing;
  printf("     ");
  for (i = 0; i < state.data.game.options.num_players; i++) {
    printf("%-20s", get(state.data.game.options.player_names, i, Name));
    move_cursor(state.data.game.options.goal * 5 + 1 -20 + 6, 0);
    /* Anzahl der Felder mal deren breite (4) +1 +Anzahl der Pins
    (gleich wie Feld), daher +5. -20 wegen MAX_NAME_LENGTH und 6 Pufferabstand */
  }
  printf("\n");
  memset(new_lines, '\n', amount_of_new_lines);
  new_lines[amount_of_new_lines] = '\0';
  puts(new_lines);
  free(new_lines);
  move_cursor(0, -amount_of_new_lines);


  for(i = 0; i < state.data.game.options.num_players; i++) {
    draw_board(state.data.game.game[i]);
    move_cursor(3 + 5 * state.data.game.options.goal, -amount_of_new_lines);
  }

  printf("\n");
  mark_selected(state.data.game.game[current_playing],
      state.data.game.selectedX,
      state.data.game.game[current_playing].outcome == RUNNING ?
        state.data.game.selectedY :
      state.data.game.selectedY - 1);

  if(state.data.game.preselect != -1){
    if (state.data.game.disclose)
      disclosing();
    else
      err_handle(multiplayer_color_selection(state.data.game.preselect));
  }

  draw_color_menu();

  if (state.data.game.msg[0] != '\0')
    popup(state.data.game.msg, 15);

  if (state.data.game.tutorial)
    print_tutorial();

  if (state.data.game.ended)
    print_end();

  if (state.data.game.pause)
    print_pause();

  return 0;
}

#define color_tutorial if (state.data.game.tutorial == 4 && state.data.game.game[0].board[y][x] != -1) state.data.game.tutorial ++;
#define tutorial_flag  if (state.data.game.tutorial == 10)  state.data.game.tutorial ++;


int check_row(int x, int y) {
  (void) x;
  return y == state.data.game.round;
}

int save_state(){
  ArrayList a = init_list(int);
  FILE *fp;
  int i = 0, j = 0;
  char filename[50] = "";
  if (state.data.game.loaded == -1)
    sprintf(filename, "gamestate-%i.mm", state.data.selection.len);
  else
    sprintf(filename, "gamestate-%i.mm", state.data.game.loaded);


  push_int(&a, state.data.game.options.rounds);
  push_int(&a, state.data.game.round);
  push_int(&a, state.data.game.options.goal);
  push_int(&a, state.data.game.options.colors);
  push_int(&a, state.data.game.options.num_players);
  push_int(&a, state.data.game.current_playing);

  for (i = 0; i < state.data.game.options.num_players; i ++){
    append(&a, get(state.data.game.options.player_names, i, Name), sizeof(Name)/sizeof(int));

    for (j = 0; j < state.data.game.options.rounds; j ++){
      append(&a, state.data.game.game[i].board[j], state.data.game.options.goal);
    }

    append(&a, state.data.game.game[i].solution, state.data.game.options.goal);
    append(&a, state.data.game.game[i].red_pins, state.data.game.options.rounds);
    append(&a, state.data.game.game[i].white_pins, state.data.game.options.rounds);
    push_int(&a, state.data.game.game[i].outcome);
    push_int(&a, sizeof(time_t)/sizeof(int));
    append(&a, &state.data.game.game[i].timestamp, sizeof(time_t)/sizeof(int));
    push_int(&a, state.data.game.game[i].bot);
  }

  for (i = 0; i < state.data.game.options.colors; i ++){
    append(&a, get(state.data.game.options.colormap, i, Color), sizeof(Color)/sizeof(int));
  }

  push_int(&a, state.data.game.ended);
  push_int(&a, state.data.game.options.counting_alg);

  fp = fopen(filename, "wb");
  fwrite(a.bytes, a.elementSize, a.len, fp);
  fclose(fp);

  destroy(a);
  return 0;
}

void start_next_round(){
  tutorial_flag;
  state.data.game.current_playing = 0;
  state.data.game.round++;
  state.data.game.selectedX = 0;
  state.data.game.selectedY = state.data.game.round;
}

int always_true(int x, int y){
  return 1 || x || y; /* ansonsten gibt es ne unused variable warning */
}

int input_end(char *input){
  if (input[0] == '\0')
    state.data.game.msg[0] = '\0';
  return 0;
}

int input_preselect(char* input){
  int a = 0;

  if (state.data.game.disclose){ /* um zu verstecken, was die andere Person eingegeben hat */
    state.data.game.disclose = 0;
    return 0;
  }

  if(all_color_input(input,
      &state.data.game.pause_selection,
      &a,
      state.data.game.options.goal,
      1,
      state.data.game.options.colors,
      &state.data.game.game[state.data.game.preselect].solution,
      always_true))
      return 0;

  if (input[0] == '.'){
    state.data.game.pause_selection = 0;
    state.data.game.disclose = 1;
    state.data.game.game[0].timestamp = time(NULL);
    state.data.game.preselect = state.data.game.preselect ? (state.data.game.preselect + 1) % state.data.game.options.num_players : -1;
  }

  return 0;
}

int input_pause(char* input){
  if (up_down_input(input, &state.data.game.pause_selection, 3))
    return 0;

  if (input[0] == '\0' && state.data.game.pause_selection == 0){
    save_state();
  }

  if (input[0] == 'p' || (input[0] == '\0' && state.data.game.pause_selection == 1)){
    state.data.game.pause = 0;
  }
  if (input[0] == '\0' && state.data.game.pause_selection == 2) {
    return 1;
  }
  return 0;

}

int write_stats(){
  int i, j;
  for (i = 0; i < state.data.game.options.num_players; i++){
    int in_list = 0;
    Name name;
    MasterMind game = state.data.game.game[i];
    int points = 0;

    if (state.data.game.options.counting_alg != 2)
      points = 100 * (((float) (1 << state.data.game.options.duplicates) * (float) state.data.game.options.goal / (float) state.data.game.options.rounds) * ((float) state.data.game.options.rounds
                  - ((float) game.lines + (float) game.timeacc / 900.0)) + (float) game.red_pins[game.lines] + ((float) game.white_pins[game.lines])/2.0);
    state.data.game.game[i].points = points;
    strcpy(name, get(state.data.game.options.player_names, i, Name));
    for (j = 0; j < state.stats.len; j ++){

      Statistics s = get(state.stats, j, Statistics);
      if (strcmp(s.name, name) == 0){
        in_list = 1;

        s.games_played ++;
        if (game.outcome == WON)
          s.games_won++;
        else if (game.outcome == LOST)
          s.games_lost++;

        s.lines_submitted += game.lines;
        s.time_spent += game.timeacc;
        s.points += points;
        s.max_points = max(s.max_points, points);
        set(state.stats, j, s, Statistics);
        break;
      }
    }

    if (!in_list){
      Statistics s;
      s.games_played = 1;
      s.games_won       = game.outcome == WON;
      s.games_lost      = game.outcome == LOST;
      s.lines_submitted = game.lines;
      s.time_spent      = game.timeacc;
      s.points          = points;
      s.max_points      = points;
      strcpy(s.name, name);
      push(state.stats, &s);
    }
  }

  return 0;
}

int write_stats_file(){
  FILE *fp = fopen("stats.txt", "wb");
  fwrite(state.stats.bytes, state.stats.len, state.stats.elementSize, fp);
  fclose(fp);
  return 0;
}

/* 2 grüne eingeben und eine is richtig, dann werden 2 weiße pins angezeigt */
void counting_pins_inp(int current_playing, int *add, int *red, int *white){
  int i, j;
  for (i = 0; i < state.data.game.options.goal; i++) {
    *add = 0;
    for (j = 0; j < state.data.game.options.goal; j++) {
      if (state.data.game.game[current_playing].board[state.data.game.round][i] == state.data.game.game[current_playing].solution[j]) {
        if (i == j) { *add = 0; (*red)++; break; }
        else { *add = 1; }
      }
    }

    if(*add)
      (*white) ++;
  }
}

/* 2 grüne eingibst und eine is richtig, dann wird nur ein weißer pin ausgegeben */
void counting_pins(int current_playing, int *add, int *red, int *white){
  int i, j, *count;
  count = calloc(state.data.game.options.colors, sizeof(int));
  for (i = 0; i < state.data.game.options.goal; i++){
    count[state.data.game.game[current_playing].solution[i]] ++;
  }

  for (i = 0; i < state.data.game.options.goal; i++) {
    int col = state.data.game.game[current_playing].board[state.data.game.round][i];
    *add = 0;

    if (col == state.data.game.game[current_playing].solution[i]){
      (*red)++;
      if (count[col] > 0)
        count[col]--;
      else
        (*white) --;
      continue;
    }

    if (count[col] <= 0) continue;

    for (j = 0; j < state.data.game.options.goal; j++) {
      if (i == j) continue;
      if (col == state.data.game.game[current_playing].solution[j] && count[col] > 0) {
        count[col]--;
        *add = 1;
        break;
      }
    }

    if(*add)
      (*white) ++;
  }
}

/* die richtige Position wird angegeben */
void counting_pins_pos(int current_playing, int *add, int *red, int *white){
  int i, j;
  (void) add;
  for (i = 0; i < state.data.game.options.goal; i++) {
    int col = state.data.game.game[current_playing].board[state.data.game.round][i];
    for (j = 0; j < state.data.game.options.goal; j++) {
      if (col == state.data.game.game[current_playing].solution[j]) {
        if (i == j)
          *red = (1 << i) | *red;
        else 
          *white = (1 << i) | *white;
      }
    }

  }
}

void (*counting_pins_alg[3])(int current_playing, int *add, int *red, int *white) = {counting_pins, counting_pins_inp, counting_pins_pos};

int input_game(char * input){
  int res = 0;
  int x = state.data.game.selectedX;
  int y = state.data.game.selectedY;
  int current_playing = state.data.game.current_playing;

  if (state.data.game.ended) return input_end(input);

  if (state.data.game.preselect != -1) return input_preselect(input);

  if (state.data.game.pause) return input_pause(input);

  if (state.data.game.tutorial > 1 && input[0] == 't'){
    state.data.game.tutorial --;
    return 0;
  }

  if (state.data.game.game[current_playing].bot == 1){
    int i;
    for (i = 0; i < state.data.game.options.goal; i++)
      state.data.game.game[current_playing].board[state.data.game.round][i] = rand() % state.data.game.options.colors;

    input[0] = '.';
  }

  if (state.data.game.game[current_playing].bot == 2){
    int i;
    for (i = 0; i < state.data.game.options.goal; i++)
      state.data.game.game[current_playing].board[state.data.game.round][i] = state.data.game.game[current_playing].solution[i];

    input[0] = '.';
  }


  switch (state.data.game.tutorial){
    case 1:
    case 2:
    case 3:
    case 5:
    case 7:
    case 8:
    case 9:
    case 11:
    case 12:
      state.data.game.tutorial ++;
      return 0;
    case 4:
      if (state.data.game.game[0].board[y][x] != -1)
        state.data.game.tutorial ++;
      break;
    case 6:
      state.data.game.tutorial ++;
      break;
    default:
      break;
  }


  if (state.data.game.select_color != -1){
    res = up_down_input(input, &state.data.game.select_color, state.data.game.options.colors);
    color_tutorial;
    if(res)
            return 0;
    if (input[0] == 0){
      color_tutorial;
      if (y == state.data.game.round)
        state.data.game.game[current_playing].board[y][x] = state.data.game.select_color;
      state.data.game.select_color = -1;
    }
  } else {
    if (state.data.game.msg[0] != '\0'){
      state.data.game.msg[0] = '\0';
      return 0;
    }

    res = all_color_input(input, &state.data.game.selectedX, &state.data.game.selectedY, state.data.game.options.goal, state.data.game.options.rounds,state.data.game.options.colors, state.data.game.game[current_playing].board, check_row);
    color_tutorial;
    if(res)
            return 0;
    if (input[0] == 0){
      state.data.game.select_color = 0;
    } else if (input[0] == '.') {
      int i, red, white, add;
      red = 0;
      white = 0;
      add = 0;
      for (i = 0; i < state.data.game.options.goal; i++) {
        if (state.data.game.game[current_playing].board[state.data.game.round][i] == -1) {
          char a[100] = "";
          sprintf(a,"The color is missing in column %i!", i + 1);
          strcpy(state.data.game.msg, a);
          return 0;
        }
      }

      (counting_pins_alg[state.data.game.options.counting_alg])(current_playing, &add, &red, &white);

      /* im Tutorial ein bisschen aushelfen */
      if (state.data.game.tutorial == 10 && red == 0){
        state.data.game.game[0].solution[0] = state.data.game.game[0].board[0][0];
        red ++;
      }

      if (state.data.game.tutorial == 10 && white == 0){
        state.data.game.game[0].solution[2] = state.data.game.game[0].board[0][3];
        white ++;
      }

      /* und böse sein */
      if (state.data.game.tutorial == 10 && red == 4){
        state.data.game.game[0].solution[0] = state.data.game.game[0].board[0][1];
        red --;
        white ++;
      }

      state.data.game.game[current_playing].red_pins[state.data.game.round] = red;
      state.data.game.game[current_playing].white_pins[state.data.game.round] = white;


      if (red == state.data.game.options.goal) {
        char timestamp[26];
        time_t timedif = time(NULL) - state.data.game.game[current_playing].timestamp + state.data.game.game[current_playing].timeacc;
        strftime(timestamp, 25, "%H:%M:%S", gmtime(&timedif));
        state.data.game.game[current_playing].outcome = WON;
        sprintf(state.data.game.msg, "You won! your time needed: %s", timestamp);

        state.data.game.game[current_playing].lines = state.data.game.round + 1;
      } else if (state.data.game.round >= state.data.game.options.rounds - 1) {
        if (state.data.game.options.num_players - 1 == current_playing)
          strcpy(state.data.game.msg, "You lost! (ono)\nTry again by pressing b");
        else
          strcpy(state.data.game.msg, "You lost! (ono)");
        state.data.game.game[current_playing].outcome = LOST;

        state.data.game.game[current_playing].lines = state.data.game.round + 1;
      }


      state.data.game.game[current_playing].timeacc += time(NULL) - state.data.game.game[current_playing].timestamp;
      state.data.game.current_playing = 1 + state.data.game.current_playing;
      if (state.data.game.current_playing >= state.data.game.options.num_players){
        start_next_round();
      }
      while (state.data.game.game[state.data.game.current_playing].outcome != RUNNING) {
        int new_round = 0;
        if (state.data.game.current_playing >= state.data.game.options.num_players){
          start_next_round();
          new_round = 1;
        }
        if (state.data.game.current_playing == current_playing && state.data.game.game[current_playing].outcome != RUNNING){
          state.data.game.ended = 1;
          write_stats();
          write_stats_file();
          break;
        }
        if (!new_round)
          state.data.game.current_playing = (state.data.game.current_playing + 1);
      }

      state.data.game.selectedX = 0;

      state.data.game.game[state.data.game.current_playing].timestamp = time(NULL);
    } else if (input[0] == 'p'){
      state.data.game.pause = 1;
    }
  }

  return 0;
}



int read_stats_file(){
  FILE *fp;
  Statistics st;
  fp = fopen("stats.txt", "rb");

  if (fp == NULL){
    return 0;
  }

  while (fread(&st, sizeof(Statistics), 1, fp) == 1)
    push(state.stats, &st);


  fclose(fp);
  return 0;
}

int init_stats(){
  state.stats = init_list(Statistics);
  state.data.stats.selection = 0;
  state.data.stats.neg = 1;
  err_handle(read_stats_file());
  return 0;
}

int destroy_stats(){
  destroy(state.stats);
  return 0;
}

#define MAX_STATS_SELECTION 7
#undef select

#define select(i) (state.data.stats.selection == (i) ? "\033[30;47m": "")





int cmp_points(Statistics *a, Statistics *b){
  int offset = state.data.stats.selection == 0? 0: state.data.stats.selection + sizeof(Name)/sizeof(int) - 1;
  return state.data.stats.neg * cmp_int((int*) a + offset, (int*) b + offset);
}

/* TODO: very inefficient */
int cmp_avg_points(Statistics *a, Statistics *b){
  float points_a = *((float*) a + 5);
  float len_a = *((float*) a + 7);
  float points_b = *((float*) b + 5);
  float len_b = *((float*) b + 7);
  float avg_a = points_a / len_a;
  float avg_b = points_b / len_b;
  return state.data.stats.neg * cmp_float(&avg_a, &avg_b);
}

int print_stats(){
  int i;
  printf("    Statistics: \n");
  printf("    \n");
  printf("  %s  Name              \033[0m  | %spoints  \033[0m | %smax   \033[0m | %sgames \033[0m | %sgames \033[0m | %sgames  \033[0m | %slines     \033[0m | %saverage \033[0m \n",
      select(0), select(1), select(2), select(3), select(4), select(5), select(6), select(7));
  printf("  %s                    \033[0m  | %s        \033[0m | %spoints\033[0m | %splayed\033[0m | %swon   \033[0m | %slost   \033[0m | %ssubmitted \033[0m | %slines   \033[0m \n",
      select(0), select(1), select(2), select(3), select(4), select(5), select(6), select(7));
  printf("                        |          |        |        |        |         |            | \n");
  for (i = 0; i < state.stats.len; i++){
    Statistics st = get(state.stats, i, Statistics);
    printf("    %-20s:    %5i |  %5i |  %5i |  %5i |   %5i |  %5i     |      %3.2f\n",
        st.name, st.points, st.max_points,
        st.games_played, st.games_won, st.games_lost,
        st.lines_submitted, (float) st.lines_submitted / (float) st.games_played );
  }
  return 0;
}

#undef select



int input_stats(char *input){
  int a = 4;
  if (all_input(input, &state.data.stats.selection, &a, MAX_STATS_SELECTION + 1, 1)){
    state.data.stats.neg = 1;
    return 0;
  }

  if (input[0] == '\0'){
    state.data.stats.neg *= -1;
    sort_by(&state.stats, state.data.stats.selection < MAX_STATS_SELECTION ? (int (*)(void* b,  void* c)) cmp_points : (int (*)(void* b,  void* c))cmp_avg_points);
  }

  return 0;
}

#define FREE_BOARD_SIZE 15


int init_free_board(){
  state.data.game.options.duplicates = 1;
  state.data.game.options.goal = 20;
  state.data.game.options.rounds = 20;
  state.data.game.options.colormap = init_list(Color);
  state.data.game.options.player_names = init_list(Name);
  append(&state.data.game.options.colormap, default_colors, 10);
  state.data.game.options.colors = state.data.game.options.colormap.len;
  state.data.free_drawing.selectedX = 0;
  state.data.free_drawing.selectedY = 0;

  init_board(&state.data.free_drawing.board, 0);

  return 0;
}

int destroy_free_board(){
  free_board(state.data.free_drawing.board);
  destroy(state.data.game.options.colormap);
  return 0;
}

int print_free_board(){
  char *buffer = malloc(50 * sizeof(char));
  memset(buffer, '\n', 49);
  buffer[49] = '\0';
  move_cursor(0, -40);
  draw_board(state.data.free_drawing.board);
  move_cursor(0, -40);

  printf("\n");
  mark_selected(state.data.free_drawing.board, state.data.free_drawing.selectedX, state.data.free_drawing.selectedY);
  free(buffer);
  return 0;
}

int input_free_board(char *input){
  all_color_input(
      input, 
      &state.data.free_drawing.selectedX, 
      &state.data.free_drawing.selectedY, 
      state.data.game.options.goal, 
      state.data.game.options.rounds,
      10,
      state.data.free_drawing.board.board,
      always_true);
  return 0;
}

#define select(X) (state.data.options.selection == (X) ? "\033[30;47m" : "")

extern char up;
extern char down;
extern char left;
extern char right;

int init_options() {
  state.data.options.selection = 0;
  return 0;
}

int input_options(char* input){
  if (up_down_input(input, &state.data.options.selection, 4)) 
    return 0;

  if (state.data.options.selection == 0 && input[0] != '\0')
    up = input[0];
  if (state.data.options.selection == 1 && input[0] != '\0')
    left = input[0];
  if (state.data.options.selection == 2 && input[0] != '\0')
    down = input[0];
  if (state.data.options.selection == 3 && input[0] != '\0')
    right = input[0];

  return 0;
}

int print_options(){
  printf("    %sUp movement: %c\033[0m\n\n"   , select(0), up);
  printf("    %sLeft movement: %c\033[0m\n\n" , select(1), left);
  printf("    %sDown movement: %c\033[0m\n\n" , select(2), down);
  printf("    %sRight movement: %c\033[0m\n\n", select(3), right);
  return 0;
}

int destroy_options(){
  return 0;
}

#undef select


int (* render[7])() = { &print_menu, &print_selection, &print_pregame, &print_game, &print_stats, &print_free_board, &print_options };
int (* handle_input[7])(char* input) = { &input_menu, &input_selection, &input_pregame, &input_game, &input_stats, &input_free_board, &input_options };
int (*init_scene[7])() = { &init_menu, &init_selection, &init_pregame, &init_game, &init_stats, &init_free_board, &init_options };
int (*destroy_scene[7])() = { &destroy_menu, &destroy_selection, &destroy_pregame, &destroy_game, &destroy_stats, &destroy_free_board, &destroy_options };



enum HelpState { NO_HELP, WANTS_HELP, HAS_HELP };
enum HelpState want_help = NO_HELP;
char **help_text;

#define MAX_INPUT_SIZE 50

int main(int argc, char **argv){
  char input[MAX_INPUT_SIZE];
  int status = 0, res;
  init_state();


  if (init_lines(&help_text, 50, 500)){
    printf("couldn't initialize help text");
  }
  parse_file("help.txt", help_text);

  setlocale(LC_ALL, "de_DE.UTF-8");
  srand(time(NULL)); /* seed für den zufalls generator setzen */
  /*system("./waitingLoop.sh &");*/
#ifdef _WIN64
  system("chcp 65001");
#endif

#if DEBUG_GAME == 1
  switch_scene(PRE_GAME);
#endif

  if (argc == 2){
    if (strcmp(argv[1], "-h") == 0){
      printf("There is no more information. Maybe you\nshould try --help?\n");
      return 0;
    }
    if (strcmp(argv[1], "--help") == 0){
      printf("This is a help message\n\n\033[1mUsage\033[0m: %s [OPTION]", argv[0]);
      printf("\n  --help - show this\n\n");
      printf("There are no other options, except for -h\n\nuse -h for more information\n");
      return 0;
    }
    if (strcmp(argv[1], "--yeet") == 0){
      printf("how did you find this?\n");
      return 0;
    }
  } else if (argc == 5) {
    printf("Yay, 5, my favorite number!\n");
    return 0;
  }

  while (input[0] != 'q'){
    printf("\033[2J");  /* Bildschirm leeren */
    (render[state.scene])();
    if (want_help == WANTS_HELP){
      want_help = HAS_HELP;
      popup(help_text[state.scene], 15);
    }

#if DEBUG == 1
    printf("\n\nYou pressed: %i:%c → \"%s\"\n", input[0], input[0], input);
    print_charr(input, MAX_INPUT_SIZE);
#endif
    printf("\n\nPress q to quit and ? for help\nYou can navigate with your arrow keys (Linux/Apple) or w/a/s/d (any platform). Press enter to confirm.\n\n\n");

    if (prompt[0] != '\0'){
      printf("%s", prompt);
      prompt[0] = '\0';
    }
    res = safe_read(input, MAX_INPUT_SIZE);

    if (res == TO_MANY_CHARS){
      sprintf(prompt, "You inserted too many characters (LIMIT OF %i)!!!", MAX_INPUT_SIZE);
      res = 0;
    } else if (res == EOF) report(EOF);

    if (want_help == HAS_HELP) {
      want_help = NO_HELP;
    } else if (input[0] == 'b' && !text_input){

      pop_scene();
    } else if (input[0] == '?' && !text_input){
      want_help = 1;
    } else {
      report((handle_input[state.scene])(input));
    }
  }

  if (status != 0 && status != 1){
    fprintf(stderr, "\nSomething went wrong, Status: %i\n", status);
  }

  cleanup();
  printf("\033[2J");
  return 0;
}
