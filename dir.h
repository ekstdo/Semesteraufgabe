#ifndef DIR_LOADED
#define DIR_LOADED

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef enum {
  NOTHING = 0,
  UP,
  DOWN,
  LEFT,
  RIGHT
} Arrow;


int up_down_input (char *input, int *value, int max);
int all_input (char *input, int *xvalue, int *yvalue, int xmax, int ymax);
int all_color_input (char *input, int *xvalue, int *yvalue, int xmax, int ymax, int max_color_value, int **board, int (*check)(int x, int y)) ;


#endif
