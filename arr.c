#include "arr.h"
#include <stddef.h>

/*
 * In dieser Datei wird die Datenstruktur ArrayList definiert (aka. automatisch erweiternde Liste)
 *
 * Wie man es verwendet:
 *
 * ArrayList a = init_list(sizeof(int));
 *
 * int ich_will_rein = 5;
 * push(a, &ich_will_rein);           fügt hinten ein Element hinzu
 *
 * int b = pop(a, sizeof(int));       entfernt das letzte Element
 *
 * int c = ((int*)a.bytes)[i];        Möglichkeit auf die einzelnen Elemente zuzugreifen
 *
 *
 */


/* initialisiert die Liste */
ArrayList init_list_(int elementSize) {
  ArrayList list;
  list.len = 0;
  list.elementSize = elementSize;
  list.cap = INIT_SIZE;
  list.bytes = malloc(INIT_SIZE * elementSize);
  return list;
}

/* fügt hinten ein Element zur Liste hinzu */



void push_(ArrayList *list, void* el){
  list->len ++;

  if (list->len > list->cap){

    list->cap = list->cap << 1;
    list->bytes = realloc(list->bytes, list->elementSize * list->cap);
  }
  memcpy(list->bytes + (list->len - 1) * list->elementSize, el, list->elementSize);
}

/* fügt hinten eine weitere Liste zur Liste hinzu */
void append(ArrayList *list, void* el_list, int n){
  int overflow = 0;
  while (list -> len + n > list -> cap){
    overflow = 1;
    list->cap = list->cap << 1;
  }

  if (overflow){
    list->bytes = realloc(list->bytes, list->elementSize * list->cap);
  }

  memcpy(list->bytes + list->len * list->elementSize, el_list, list->elementSize * n);
  list->len += n;
}

/* shortcut, um integer zu pushen */
void push_int(ArrayList *list, int el){
  push_(list, &el);
}

/* holt sich das letzte Element aus der Liste */
void *pop_(ArrayList *list){
  return list->bytes + --(list->len) * list->elementSize;
}

/* ULTIMATIVE DESTRUCTION */
void destroy(ArrayList list){
  free(list.bytes);
}

/* klont eine Liste */
ArrayList clone(ArrayList *list){
  ArrayList new_list;
  new_list.len = list->len;
  new_list.elementSize = list->elementSize;
  new_list.cap = list->cap;
  new_list.bytes = malloc(list->elementSize * list->cap);
  memcpy(new_list.bytes, list-> bytes, list->elementSize * list->cap);
  return new_list;
}


int swap(ArrayList *list, int a, int b){
	char* carry = malloc(list->elementSize);
	if (carry == NULL){
		free(carry);
		return 1;
	}

	memcpy(carry, list->bytes + a * list->elementSize, list->elementSize);
	memcpy(list->bytes + a * list->elementSize, list->bytes + b * list->elementSize, list->elementSize);
	memcpy(list->bytes + b * list->elementSize, carry, list->elementSize);

	free(carry);

	return 0;
}

/* Schaut ob ein Element in ner Liste ist */
int in(ArrayList *list, void* el){
  int i = 0;
  for (i = 0; i < list->len; i ++){
    if(memcmp(list->bytes + i * list->elementSize, el, list->elementSize) == 0)
      return 1;
  }
  return 0;
}

/* Schaut ob ein String in ner Liste ist */
int str_in(ArrayList *list, char* el){
  int i = 0;
  for (i = 0; i < list->len; i ++){
    if(strcmp(list->bytes + i * list->elementSize, el) == 0)
      return 1;
  }
  return 0;
}

#define malloc_handle(X) {if(X == NULL){free(X); return 1;}}
int partition(ArrayList *list, int lower, int upper, int (*cmp)(void* a, void *b)){
  int lswap = lower;
  int rswap = upper - 1;

 while(lswap < rswap){
    while(lswap < upper && cmp(list->bytes + lswap * list->elementSize, list->bytes + upper * list->elementSize) == -1)
      lswap ++;

    while(rswap > lower && cmp(list->bytes + rswap * list->elementSize, list->bytes + upper * list->elementSize) > -1)
      rswap --;

    if (lswap < rswap)
      swap(list, lswap, rswap);
  }

  if (cmp(list->bytes + lswap * list->elementSize, list->bytes + upper * list->elementSize) == 1)
    swap(list, lswap, upper);
  return lswap;
}

void quick_sort(ArrayList *list, int lower, int upper, int (*cmp)(void* a, void *b)){
  if (lower < upper){
    int index = partition(list, lower, upper, cmp);
    quick_sort(list, lower, index - 1, cmp);
    /* das pivot element ist schon sortiert */
    quick_sort(list, index + 1, upper, cmp);
  }
}

/* implementing in place quick sort */
void sort_by(ArrayList *list, int (*cmp)(void* a, void *b)){
  quick_sort(list, 0, list->len - 1, cmp);
}

#include <stdio.h>

int cmp_int(int *a, int *b){
  int ai = *a;
  int bi = *b;
  return ai < bi ? -1 : ai == bi ? 0 : 1;
}

int cmp_float(float *a, float *b){
  float ai = *a;
  float bi = *b;
  return ai < bi ? -1 : ai == bi ? 0 : 1;
}


/*
int main(){
  ArrayList a = init_list(int);

  int x = 5;
  int y = 7;

  push(a, &x);
  push(a, &y);
  printf("%i,\n", cmp_int(&y, &x));

  printf("got from list: %i\n", get(a, 0, int)); // a[0]

  set(a, 0, 55, int); // a[0] = 55;

  printf("after setting it to 55: %i\n", get(a, 0, int));

  printf("is 7 in a? %i\n", in(&a, &y));

  printf("pop off the last element: %i\n", pop(a, int));

  ArrayList b = clone(&a);
  int c[40] = {10, 15, 12, 8, 3, 15, 12, 8, 3, 91, 2, 94, 83, 823, 28, 382, 371, 3732, 732, 10, 15, 12, 8, 3, 15, 12, 8, 3, 91, 2, 94, 83, 823, 28, 382, 371, 3732, 732, 10, 15};

  append(&b, c, 40);

  printf("pop off the last element: %i\n", pop(b, int));

  sort_by(&b, cmp_int);

  for (x = 0; x < b.len; x ++){
    printf("The %ith Element of b is: %i\n", x, get(b, x, int));
  }

  destroy(a);
  destroy(b);
}
*/
